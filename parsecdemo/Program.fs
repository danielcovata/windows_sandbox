﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

open FParsec

type UserState = unit
type Parser<'t> = Parser<'t, UserState>

let test p str =
  match run p str with
  | Success(result, _, _) -> printfn "Success: %A" result
  | Failure(errorMsg, _, _) -> printfn "failure: %s" errorMsg



let str s = pstring s
let floatBetweenBrackets : Parser<_> = str "[" >>. pfloat .>> str "]"
 
let betweenStrings s1 s2 p = p |> between (str s1) (str s2)

let floatList : Parser<_> = str "[" >>. sepBy pfloat (str ",") .>> str "]"

let ws = spaces

let str_ws s = pstring s .>> ws
let float_ws : Parser<_> = pfloat .>> ws
let numberList : Parser<_> = str_ws "[" >>. sepBy float_ws (str_ws ",") .>> str_ws "]"

let identifier = 
    let isIdentifierFirstChar c = isLetter c || c = '_'
    let isIdentifierChar c = isLetter c || isDigit c || c = '_'

    many1Satisfy2L isIdentifierFirstChar isIdentifierChar "identifier" .>> ws


let stringLiteral : Parser<_> =
    let normalChar = satisfy (fun c -> c <> '\\' && c <> '"')
    let unescape c = match c with
                     | 'n' -> '\n'
                     | 'r' -> '\r'
                     | 't' -> '\t'
                     | c   -> c
    let escapedChar = pstring "\\" >>. (anyOf "\\nrt\"" |>> unescape)
    between (pstring "\"") (pstring "\"")
            (manyChars (normalChar <|> escapedChar))

// snippet by snippet
let stringLiteral2 : Parser<_> =
    let normalCharSnippet = manySatisfy (fun c -> c <> '\\' && c <> '"')
    let escapedChar = pstring "\\" >>. (anyOf "\\nrt\"" |>> function
                                                            | 'n' -> "\n"
                                                            | 'r' -> "\r"
                                                            | 't' -> "\t"
                                                            | c   -> string c)
    between (pstring "\"") (pstring "\"")
        (stringsSepBy normalCharSnippet escapedChar)
            //(manyStrings (normalCharSnippet <|> escapedChar))


let matchString p str =         
    match run floatList str with
        | Success(result, _, _) -> result
        | Failure(errorMsg, _, _) -> failwith "error parsing"

[<EntryPoint>]
let main argv = 
    
    test pfloat "1.25"
  
    test floatBetweenBrackets "[3.4434]"
    test floatBetweenBrackets "[1.0"

    test (pfloat |> betweenStrings "&&" "&&") "&&787.878&&"
    test (pfloat |> betweenStrings "xX" "Xx") "xX787.878Yx"

    test (many floatBetweenBrackets) ""
    test (many floatBetweenBrackets) "[2][3][5]"

    test (many floatList) "[1,2,3,4]"
    let y = matchString floatList "[1,2,3,4]" 
    test (many1 floatList <?> "list of floats") "[1 2X3,4]"

    test (many (str "a" <|> str "b")) "abba"

    test identifier "_test8"
    test identifier "8foo"

    test stringLiteral "\"foo\nbar\jqux\""

    test stringLiteral2 "\"foo\nbar\rqux\xjolly\""

    System.Console.ReadKey() |> ignore
    0 // return an integer exit code
