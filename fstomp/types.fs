﻿namespace fstomp
   
    type Ack = 
        | Auto
        | Client
        | ClientIndividual

    type ServerCommand =                 
        | Connected
        | Message
        | Receipt
        | Error


        //TODO - get f# 3.1 for named union fields
    type ClientCommand =
        | Send of string * string           // destination * body
         //TODO: Subscribe Ack type
        | Subscribe of string * string       //id * destination * ack , ie /queue/foo
        | Unsubscribe of string             // subscription id
        | Begin of string                   // transaction id
        | Commit of string                        // transaction-id
        | Abort of string                           // transaction-id
        | Ack of string * string            // message-id * transaction-id
        | Nack of string * string           // message-id * transaction-id
        | Disconnect                        // receipt ??
        | Connect of string * bool                          // hostname * heartbeat 
        | Stomp                             // TBD - not documented in http://stomp.github.io/stomp-specification-1.2.html


    type Header = { Name : string; Value : string }


    //TODO - make this simpler
    type ClientMessage = 
        { ClientCommand: ClientCommand; Headers: Header list; Body : string }
        override this.ToString() = sprintf "%A %A Body %s" this.ClientCommand this.Headers this.Body

    
    type ServerMessage = 
        { ServerCommand: ServerCommand; Headers: Header list; Body : string }
        override this.ToString() = sprintf "%A %A Body %s" this.ServerCommand this.Headers this.Body