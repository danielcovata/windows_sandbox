﻿namespace fstomp

module stompparse = 

    open FParsec

    type UserState = unit
    type Parser<'t> = Parser<'t, UserState>
            
    
    let sframe : Parser<_> = 
        let str s = pstring s
        let ws = spaces
        let isStompChar c = c <> ':' && c <> '\r' && c <> '\n'

        let sheaderName = many1Satisfy isStompChar
        let sheaderValue = manySatisfy isStompChar

        let scommand : Parser<_> =  
            [stringReturn "CONNECTED" fstomp.Connected
             stringReturn "MESSAGE" fstomp.Message
             stringReturn "RECEIPT" fstomp.Receipt
             stringReturn "ERROR" fstomp.Error] 
            |> choice
        let sheader : Parser<_> = sheaderName .>>. (ws >>. str ":" >>. ws >>. sheaderValue) 
        let sheaderEol = sheader .>> newline
        let sheaderList = many sheaderEol
        let sbody : Parser<_> = many1Satisfy (fun c -> c <> char 0)
        scommand .>> newline .>>. sheaderList .>> skipNewline .>>. sbody .>> skipChar (char 0)  .>> many skipNewline .>> eof
