﻿namespace fstomp

module Parser = 
    open stompparse
    open FParsec
    
    // (Command * (string * string) list ) * string
    let Parse message = 
        match run sframe message with
        | Success(((command, headers), body), _, _) -> 
            { ServerCommand = command
              Headers = 
                  List.map (fun (k, v) -> 
                      { Name = k
                        Value = v }) headers
              Body = body }
        | Failure(errorMsg, _, _) -> invalidArg "message" errorMsg
