﻿
open FParsec
open fstomp.stompparse

let test p str =
  match run p str with
  | Success(result, _, _) -> printfn "Success: %A" result
  | Failure(errorMsg, _, _) -> printfn "failure: %s" errorMsg


[<EntryPoint>]
let main argv = 
    printfn "\nTesting frames"
    test sframe "SEND\n\nRandomDataHere\000\n"
    test sframe "CONNECTED\nfoo_bar:abc123\n\n8493fdasfdaaaxxx\000\n"
    test sframe "CONNECTED\nfoo_bar:abc123\nAuthorization Filter: Space Junk 99\n\n8493 fdasfd \n\n\n2332 aaaxxx\"\000\n"

    System.Console.ReadKey() |> ignore
    
    0 // return an integer exit code
