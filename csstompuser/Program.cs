﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Covata.Api;
using Covata.Api.OAuth.Credentials;
using Covata.Api.OAuth.GrantTypes;
using fstomp;
using Microsoft.SqlServer.Server;
using WebSocket4Net;
using System.IO;
using ErrorEventArgs = SuperSocket.ClientEngine.ErrorEventArgs;

namespace csstompuser
{
    public class StompClient
    {     
        //TODO - tied to Websockets since that's all we need..

        public StompClient(Uri server, string accessToken)
        {
            _websocket = CreateWebsocket(server.Host, accessToken);
            _websocket.Opened  += WebsocketOnOpened;
            _websocket.Closed += WebsocketOnClosed;
            _websocket.Error += WebsocketOnError;
            _websocket.MessageReceived += WebsocketOnMessageReceived;
            _websocket.Open();
        }

        private void WebsocketOnMessageReceived(object sender, MessageReceivedEventArgs messageReceivedEventArgs)
        {
            // need to first parse the STOMP message and extract the actions
            //            var action = Json.Deserialize<ActionDto>(e.Message);
        }

        private void WebsocketOnError(object sender, ErrorEventArgs errorEventArgs)
        {
            
        }

        private void WebsocketOnOpened(object sender, EventArgs eventArgs)
        {
            //var s = Formatter.format("qa.sec.covata.com", true);

        }

        private void WebsocketOnClosed(object sender, EventArgs eventArgs)
        {
            
        }

        private WebSocket CreateWebsocket(string hostname, string accessToken)
        {
            var wsUri = new Uri(String.Format("ws://{0}/api/v1/items/ws/abcd/12345678/websocket", hostname));
            var headers = new Dictionary<string, string>() { { "Authorization", "Bearer " + accessToken } }.ToList();
            return new WebSocket(wsUri.OriginalString, customHeaderItems: headers);
        }
        
        private static WebSocket _websocket;
        public const string Version = "1.2";
    }

    class Program
    {
        static void Main(string[] args)
        {

            try
            {
           //     var message = "CONNECTED\nfoo_bar:abc123\nanother_header:another header value\n\nThisIsALoooooooooooooooooooooooooooooooooooongBody\0\n";
            ///    var result = Parser.parse(message);

                var serverUrl = new Uri("http://qa.sec.covata.com");
                ICovataApiClient client = new CovataApiClient(serverUrl.OriginalString, new GrantPassword("client1", "secret1"));
                            var credentials = new PasswordCredentials("user1@test.covata.com", "password1");

                if (!client.Authorize(credentials))
                {
                    throw new Exception("Unauthorized");
                }
                var accessToken = client.GetAccessToken();

                var s = fstomp.Formatter.format(ClientCommand.NewConnect(serverUrl.Host, true));
                var s2 = fstomp.Formatter.format(ClientCommand.NewAbort("tx1"));
                var s3 = Formatter.format(ClientCommand.NewAck("12345", "tx1"));
                var s4 = Formatter.format(ClientCommand.NewBegin("tx1"));
                var s5 = Formatter.format(ClientCommand.NewCommit("tx1"));
                var s6 = Formatter.format(ClientCommand.NewNack("12345", "tx1"));
                var s7 = Formatter.format(ClientCommand.NewSend("/queue/foo", "My Data"));
                var s8 = Formatter.format(ClientCommand.NewSubscribe("12345", "/queue/foo"));
                var s9 = Formatter.format(ClientCommand.NewUnsubscribe("12345"));
                
                var stomp = new StompClient(serverUrl , accessToken);

            }
            catch (Exception e)
            {

                System.Console.WriteLine("exception: {0}", e.ToString());
            }
            System.Console.ReadKey();
        }
    }
}
